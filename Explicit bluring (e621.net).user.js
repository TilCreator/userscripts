// ==UserScript==
// @name         Explicit bluring (e621.net)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Explicit%20bluring%20(e621.net).user.js
// @author       TilCreator
// @match        https://e621.net/*
// @match        https://e926.net/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
.post-preview {
    overflow: visible !important;
}
.post-preview[data-rating=e] img, .post-preview[data-rating=q] img {
    filter: blur(10px);

    transition: filter .2s;
}

.post-preview:hover img {
    filter: none;
}
`);