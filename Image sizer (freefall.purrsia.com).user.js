// ==UserScript==
// @name         Image sizer (freefall.purrsia.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(freefall.purrsia.com).user.js
// @author       TilCreator
// @match        http://freefall.purrsia.com/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('body { background: none !important; }');

(function() {
    handle_webcomic(document.querySelector('body > img:nth-of-type(1)'), document.querySelector('body > a:nth-of-type(1)'), document.querySelector('body > a:nth-of-type(3)'), false);
})();
