// ==UserScript==
// @name         Image sizer (housepetscomic.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(housepetscomic.com).user.js
// @author       TilCreator
// @match        http*://www.housepetscomic.com/comic/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('#comic img {max-width: 99vw;max-height: 300vh; margin: auto;object-fit: contain;}\
.mh-container.mh-container-outer {margin: 0px;}\
body {background: black !important;}\
.mh-container.mh-container-inner.mh-row.clearfix, .vm-placement {display: none;}\
.mh-container-outer {max-width: none !important;}');

(function() {
    handle_webcomic(document.querySelector('#comic img'), document.querySelector('.navi.comic-nav-previous.navi-prev'), document.querySelector('.navi.comic-nav-next.navi-next'));
})();
