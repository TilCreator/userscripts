// ==UserScript==
// @name         Image sizer (feretta.net)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(feretta.net).user.js
// @author       TilCreator
// @match        http*://www.feretta.net/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('#comic img {max-width: 100vw;max-height: 200vh;}');

(function() {
    scrollTo(0, document.querySelector('#comic img').getBoundingClientRect().y);
})();
