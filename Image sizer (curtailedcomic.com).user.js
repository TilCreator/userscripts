// ==UserScript==
// @name         Image sizer (curtailedcomic.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(curtailedcomic.com).user.js
// @author       TilCreator
// @match        https://www.curtailedcomic.com/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
* {
  background: #000 !important;
  color: #fff;
}

#comic img {
  /*filter: invert(1);*/
  width: 100%;
  /*max-height: 100vh;*/
  object-fit: contain;
}

#comic-wrap {
  padding: 0px;
}
`);

(function() {
    scrollTo(0, document.querySelector('#comic img').getBoundingClientRect().y);

    document.onkeydown = function (e) {
        e = e || window.event;

        console.log(e.keyCode);

        if (e.keyCode == '37' || e.keyCode == '68') {
            console.log('test');
            document.querySelector('.comic-nav-previous').click();
        } else if (e.keyCode == '39' || e.keyCode == '70') {
            document.querySelector('.comic-nav-next').click();
        }
    }
})();