// ==UserScript==
// @name         Masterdon full size image hack
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Masterdon%20full%20size%20image%20hack.user.js
// @author       TilCreator
// @match        *://*/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.media-gallery, .media-gallery__item {height: auto !important;}');
