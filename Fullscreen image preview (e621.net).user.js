// ==UserScript==
// @name         Fullscreen image preview (e621.net)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Fullscreen%20image%20preview%20(e621.net).user.js
// @author       TilCreator
// @match        https://e621.net/*
// @match        https://e926.net/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
.thumbnail a {
    height: 150px;
    width: 154px;
    overflow: hidden;
}


.thumbnail:hover img {
    position: fixed;
    width: 100%;
    max-width: 100%;
    height: 100%;
    max-height: 100%;
    top: 0%;
    left: 0%;
    z-index: 10;

    object-fit: contain;

    pointer-events: none;

    filter: none;

    opacity: 0.6;
    animation: fade_in 0.5s forwards;
}

@keyframes fade_in {
    from {
        opacity: 0.6;
    }
    90% {
        opacity: 0.6;
    }
    to {
        opacity: 1;
    }
}

.thumbnail:hover a::after {
    content: 'img';

    display: block;
    height: 150px;
    width: 154px;
}

#notice, #error {
    z-index: 15;
    opacity: 0.6;
}
`);

(function() {
    'use strict';

    document.querySelectorAll('.thumbnail').forEach(function(value, index) {
        value.addEventListener("mouseenter", function(e) {
            this.querySelector('img').addEventListener('error', function(e) {
                // Gets real sample url
                var xmlHttp = new XMLHttpRequest();
                xmlHttp.e = this
                xmlHttp.onreadystatechange = function() {
                    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                        if (this.e.getAttribute('resrc') == 'guessed_sample') {
                            this.e.src = JSON.parse(xmlHttp.responseText).post.sample.url;
                            this.e.setAttribute('resrc', 'sample');
                        } else if (this.e.getAttribute('resrc') == 'sample') {
                            this.e.src = JSON.parse(xmlHttp.responseText).post.file.url;
                            this.e.setAttribute('resrc', 'file');
                        } else if (this.e.getAttribute('resrc') == 'file') {
                            this.e.src = JSON.parse(xmlHttp.responseText).post.preview.url;
                            this.e.setAttribute('resrc', 'preview');
                        }
                    }
                }
                xmlHttp.open("GET", 'https://e621.net/posts/' + this.parentNode.parentNode.parentNode.attributes['data-id'].value + '.json', true);
                xmlHttp.send(null);
            }, false);

            if (this.querySelector('img').hasAttribute('resrc')) {
                return;
            }

            this.querySelector('img').src = this.querySelector('source').srcset;

            // Might be enough to get sample url
            this.querySelector('img').src = this.querySelector('source').srcset.replace('preview', 'sample').replace('crop', 'sample');
            this.querySelector('img').setAttribute('resrc', 'guessed_sample');

            this.querySelectorAll('source').forEach(function(value, index) {
                value.parentNode.removeChild(value);
            });
        }, false);
    });
})();
