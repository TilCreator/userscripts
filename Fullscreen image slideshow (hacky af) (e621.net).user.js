// ==UserScript==
// @name         Fullscreen image slideshow (hacky af) (e621.net)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Fullscreen%20image%20preview%20(e621.net).user.js
// @author       TilCreator
// @match        https://e621.net/*
// @match        https://e926.net/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
body {
    overflow: hidden;
    cursor: none;
}

#page  {
    width: 100px;
    margin-bottom: -130px !important;
    background: black !important;
}

article.post-preview {
    height: calc(100vh - 48px) !important;
    width: 100vw !important;
}

#page-footer, .paginator, #top, #sidebar, .desc {
    display: none;
}

#content {
    margin: 0px !important;
}

.post-preview a {
    height: 150px;
    width: 154px;
    overflow: hidden;
}


.post-preview:hover img {
    position: fixed;
    width: 100%;
    max-width: 100%;
    height: 100%;
    max-height: 100%;
    top: 0%;
    left: 0%;
    z-index: 10;

    object-fit: contain;

    pointer-events: none;

    filter: none;

    background: black;
}

@keyframes fade_in {
    from {
        opacity: 0.6;
    }
    90% {
        opacity: 0.6;
    }
    to {
        opacity: 1;
    }
}

.post-preview:hover a::after {
    content: 'img';

    display: block;
    height: 150px;
    width: 154px;
}

#notice, #error {
    z-index: 15;
    opacity: 0.6;
}
`);

(function() {
    'use strict';
    
    var handle_load_error = function(e) {
        // Gets real sample url
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.e = e.target;
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4) {
                if (xmlHttp.status == 200) {
                    this.e.src = JSON.parse(xmlHttp.responseText).post.file.url;
                    this.e.setAttribute('resrc', 'file');
                    console.log("Got", JSON.parse(xmlHttp.responseText).post.id);
                    /*if (this.e.getAttribute('resrc') == 'guessed_sample') {
                        this.e.src = JSON.parse(xmlHttp.responseText).post.sample.url;
                        this.e.setAttribute('resrc', 'sample');
                    } else if (this.e.getAttribute('resrc') == 'sample') {
                        this.e.src = JSON.parse(xmlHttp.responseText).post.file.url;
                        this.e.setAttribute('resrc', 'file');
                    } else if (this.e.getAttribute('resrc') == 'file') {
                        this.e.src = JSON.parse(xmlHttp.responseText).post.preview.url;
                        this.e.setAttribute('resrc', 'preview');
                    }*/
                } else {
                    handle_load_error({"target": e});
                }
            }
        }
        xmlHttp.open("GET", 'https://e621.net/posts/' + e.target.parentNode.parentNode.parentNode.attributes['data-id'].value + '.json', true);
        xmlHttp.send(null);
        console.log("Getting", e.target.parentNode.parentNode.parentNode.attributes['data-id'].value);
    }

    setTimeout(function () {
        document.querySelectorAll('.post-preview').forEach(function(e, index) {
            /*e.querySelector('img').addEventListener('error', handle_load_error, false);

            if (e.querySelector('img').hasAttribute('resrc')) {
                return;
            }

            e.querySelector('img').src = e.querySelector('source').srcset;

            // Might be enough to get sample url
            //e.querySelector('img').src = e.querySelector('source').srcset.replace('preview', 'sample').replace('crop', 'sample');
            //e.querySelector('img').setAttribute('resrc', 'guessed_sample');
            e.querySelector('img').src = e.querySelector('source').srcset.replace('preview', '').replace('crop', '');
            e.querySelector('img').setAttribute('resrc', 'guessed_file');*/

            e.querySelectorAll('source').forEach(function(value, index) {
                value.parentNode.removeChild(value);
            });

            console.log(index);
            var element = {"target": e.querySelector('img')};
            setTimeout(function () {
                handle_load_error(element);
            }, 1000 * index);
        }, false);
    }, 7000);
})();
