// ==UserScript==
// @name         Bandcamp volume control
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Bandcamp%20volume%20control.user.js
// @author       TilCreator
// @match        https://*.bandcamp.com/*
// @match        https://lapfoxtrax.com/*
// @match        https://mumbleetc.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    setInterval(function() {
        document.querySelectorAll('audio[src]').forEach(function(e) {
            e.controls = true;
            e.volume = JSON.parse(localStorage.getItem('volume'));
            e.style = 'position: fixed; top: 53px; left: 0px;';
            e.onvolumechange = function(e) {
                localStorage.setItem('volume', JSON.stringify(e.srcElement.volume))
            };
        });
    },
    100);
})();
