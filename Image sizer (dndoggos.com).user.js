// ==UserScript==
// @name         Image sizer (dndoggos.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(dndoggos.com).user.js
// @author       TilCreator
// @match        http*://dndoggos.com/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
.comic-image {
  filter: invert(1);
}
`);

(function() {
    handle_webcomic(document.querySelector('.comic-image'), document.querySelector('[ng-click="ctrl.previous()"]'), document.querySelector('ng-click="ctrl.next()"'));
})();
