// ==UserScript==
// @name         Image sizer (silvercomics.online)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(silvercomics.online).user.js
// @author       TilCreator
// @match        http*://silvercomics.online/*
// @grant        GM_addStyle
// ==/UserScript==


GM_addStyle(`
body {
    background: #000 !important;
}

#primary, main, main > article img {
    margin: auto !important;
    max-width: none !important;
    width: 100%;
}

#secondary {
    display: none;
}
`);

(function() {
    document.querySelectorAll('main > article img').forEach((e) => {
        console.log(e);
        e.src = e.src.replace(/(.*)-[0-9]+x[0-9]+(.*)/, "$1$2");
        e.removeAttribute("srcset");
    });

    scrollTo(0, document.querySelector('main').getBoundingClientRect().y);
})();
