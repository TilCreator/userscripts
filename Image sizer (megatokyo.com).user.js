// ==UserScript==
// @name         Image sizer (megatokyo.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(megatokyo.com).user.js
// @author       TilCreator
// @match        https://megatokyo.com/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('#strip-bl > img:nth-child(1) {max-width: 95vw;max-height: 200vh; width: 100vw !important; height: auto !important; margin: auto;}');

(function() {
    scrollTo(0, document.querySelector('#strip-bl > img:nth-child(1)').getBoundingClientRect().y);

    document.onkeydown = function (e) {
        e = e || window.event;

        if (e.keyCode == '37' || e.keyCode == '68') {
            document.querySelector('.prevnext > .prev > a').click();
        } else if (e.keyCode == '39' || e.keyCode == '70') {
            document.querySelector('.prevnext > .next > a').click();
        }
    }
})();