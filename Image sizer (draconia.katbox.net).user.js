// ==UserScript==
// @name         Image sizer (draconia.katbox.net)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(draconia.katbox.net).user.js
// @author       TilCreator
// @match        http://draconia.katbox.net/comic/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.webcomic-image > a:nth-child(1) > img:nth-child(1) {max-width: 100vw;max-height: 100vh;}');

(function() {
    scrollTo(0, document.querySelector('.webcomic-image > a:nth-child(1) > img:nth-child(1)').getBoundingClientRect().y);

    document.onkeydown = function (e) {
        e = e || window.event;

        if (e.keyCode == '37' || e.keyCode == '68') {
            document.querySelector('#webcomiclink-4 > a:nth-child(1)').click();
        } else if (e.keyCode == '39' || e.keyCode == '70') {
            document.querySelector('#webcomiclink-5 > a:nth-child(1)').click();
        }
    }
})();