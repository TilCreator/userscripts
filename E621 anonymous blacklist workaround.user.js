// ==UserScript==
// @name        E621 anonymous blacklist workaround
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/E621%20anonymous%20blacklist%20workaround.user.js
// @match       https://e621.net/posts/*
// @grant       none
// @author      TilCreator
// ==/UserScript==

// How to reconstruct an image link:
// needed: md5 hash and file extention
// to reconstruct: https://static1.e621.net/data/ + first and second char of md5 + / + third and fourth char of md5 + / + complete md5 + . + file extention
// or in js: 'https://static1.e621.net/data/' + post.post.file.md5.slice(0, 2)  + '/' + post.post.file.md5.slice(2, 4) + '/' + post.post.file.md5 + '.' + post.post.file.ext
//
// to get preview, sample or cropped urls, add those after /data and change the file extention to "jpg"
// so https://static1.e621.net/data/preview/ ... .jpg for example


(function() {
    'use strict';

    if (document.querySelector('#image-container > p:nth-child(3)').innerHTML == "You must be logged in to view this image.") {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var post = JSON.parse(xmlHttp.responseText);
                console.log(post);
                var src = 'https://static1.e621.net/data/' + post.post.file.md5.slice(0, 2)  + '/' + post.post.file.md5.slice(2, 4) + '/' + post.post.file.md5 + '.' + post.post.file.ext;
                console.log(src);

                document.querySelector('#image-container').innerHTML = '<img id="image" src="' + src + '"/>';
                document.querySelector('#image-container').classList.remove('blacklisted-active');
            }
        }
        xmlHttp.open("GET", 'https://e621.net/posts/' + document.querySelector('meta[name=post-id]').getAttribute('content') + '.json', true);
        xmlHttp.send(null);
    }
})();
