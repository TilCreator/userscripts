// ==Script==
// @name         webcomic-helper
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @author       TilCreator
// ==/Script==

function handle_webcomic(image, prev, next, upscaling=true, precaching=true, scale=2, noise=3, scroll=true) {
    function inIframe () {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    if (upscaling === true) {
        image.src = 'https://tilpad.tils.pw/waifu2xod?scale=' + scale + '&noise=' + noise + '&url=' + image.src;
    }

    if (!inIframe()) {
        if (scroll === true) {
            image.scrollIntoView();
            image.onload = function(e) {
                e.srcElement.scrollIntoView();
            }
        }

        if (precaching === true) {
            var ifr = document.createElement('iframe');
            ifr.src = prev.href;
            document.querySelector('body').appendChild(ifr);
            ifr = document.createElement('iframe');
            ifr.src = next.href;
            document.querySelector('body').appendChild(ifr);
        }

        document.onkeydown = function (e) {
            e = e || window.event;

            if (e.keyCode == '37' || e.keyCode == '68') {
                prev.click();
            } else if (e.keyCode == '39' || e.keyCode == '70') {
                next.click();
            }
        }
    }
}
