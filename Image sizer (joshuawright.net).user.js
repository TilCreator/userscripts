// ==UserScript==
// @name         Image sizer (joshuawright.net)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(joshuawright.net).user.js
// @author       TilCreator
// @match        https://www.joshuawright.net/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// ==/UserScript==

(function() {
    handle_webcomic(document.querySelector('img.block:not([src*="images/banner"])'), document.querySelector('a[href*=slack-]:not([href*=slack-wiki]):first-of-type'), document.querySelector('a[href*=slack-]:not([href*=slack-wiki]):last-of-type'), false);
})();
