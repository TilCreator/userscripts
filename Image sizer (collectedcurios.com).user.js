// ==UserScript==
// @name         Image sizer (collectedcurios.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(collectedcurios.com).user.js
// @author       TilCreator
// @match        https://www.collectedcurios.com/sequentialart.php*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.w3-display-middle {width: 100% !important;}');

(function() {
    handle_webcomic(document.querySelector('.w3-display-middle > .w3-image'), document.querySelector('#backOne'), document.querySelector('#forwardOne'));
})();
