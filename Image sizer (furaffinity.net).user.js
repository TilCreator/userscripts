// ==UserScript==
// @name         Image sizer (furaffinity.net)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(furaffinity.net).user.js
// @author       TilCreator
// @match        https://www.furaffinity.net/view/*
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.imgresizer{max-width: calc(100vw - 60px);max-height: calc(100vh - 100px);}');

(function() {
    scrollTo(0, document.querySelector('#page-submission').getBoundingClientRect().top);

    document.onkeydown = function (e) {
        e = e || window.event;

        if (e.keyCode == '37' || e.keyCode == '68') {
            document.querySelector('.next.button-link').click();
        } else if (e.keyCode == '39' || e.keyCode == '70') {
            document.querySelector('.prev.button-link').click();
        } else if (e.keyCode == '32') {
            e.preventDefault();
            document.querySelector('div.alt1 > b:nth-child(2) > a:nth-child(1)').click();
        }
    }
})();