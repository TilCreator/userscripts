// ==UserScript==
// @name         flash downloader
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/flash%20downloader.user.js
// @author       TilCreator
// @description  Add download button (external player)
// @match        https://www.comdotgame.com/play/*
// ==/UserScript==

(function() {
    'use strict';

    document.getElementById("flash-game").innerHTML = `
        <a href=${document.getElementById("object").data}>get</a>
    `;
    document.getElementById("flash-game").style.width = "auto";
    document.getElementById("flash-game").style.height = "auto";
    document.getElementById("flash-game").style.background = "white";
    document.getElementById("flash-game").style["text-align"] = "center";
})();