// ==UserScript==
// @name         Image sizer (ah-club)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(ah-club).user.js
// @author       TilCreator
// @match        https://rickgriffinstudios.com/comic-post/ah-club*/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.x-container.max.width, #top {width: auto;max-width: none;}');

(function() {
    handle_webcomic(document.querySelector('#comic img'), document.querySelector('.navi-prev-in'), document.querySelector('.navi-next-in'));
})();
