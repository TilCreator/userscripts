// ==UserScript==
// @name         Image sizer (foxdad.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(foxdad.com).user.js
// @author       TilCreator
// @match        https://foxdad.com/post/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('nav#access {position:relative !important;}');

(function() {
    handle_webcomic(document.querySelector('article.post img'), document.querySelector('.previous-button'), document.querySelector('.next-button'), false);
})();
