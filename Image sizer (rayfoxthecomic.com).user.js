// ==UserScript==
// @name         Image sizer (rayfoxthecomic.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(rayfoxthecomic.com).user.js
// @author       TilCreator
// @match        http*://www.rayfoxthecomic.com/comic/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
#menubar-wrapper {
    display: none !important;
}
#sidebar-right-of-comic {
  display: block !important;
  width: auto !important;
}
#comic {
  display: block !important;
}
`);

(function() {
    document.querySelector('#comic img').src = document.querySelector('#comic img').src.split('?')[0];

    handle_webcomic(document.querySelector('#comic img'), document.querySelector('.navi.comic-nav-previous.navi-prev'), document.querySelector('.navi.comic-nav-next.navi-next'));
})();
