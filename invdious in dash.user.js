// ==UserScript==
// @name        invdious in dash
// @match       https://vid.mint.lgbt/watch?*
// @grant       none
// @version     0.0
// @author      TilCreator
// ==/UserScript==

(function() {
    if (location.href.startsWith("https://vid.mint.lgbt/watch?v=") && !location.href.includes("&quality=dash")) {
        location.href = location.href + "&quality=dash";
    }
})();