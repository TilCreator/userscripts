// ==UserScript==
// @name         Image sizer (the-whiteboard.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(the-whiteboard.com).user.js
// @author       TilCreator
// @match        http*://*the-whiteboard.com/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
body {
  filter: invert(1);
}
body > table:nth-child(1) {
  display: none;
}
body > center:nth-child(2) {
  display: none;
}
body > center:nth-child(3) > a:nth-child(2) > img:nth-child(2) {
  filter: invert(1);
  min-width: 70%;
}
body > center:nth-child(3) > table:nth-child(12) img, body > center:nth-child(3) > img {
  filter: invert(1);
}
a {
  color: rgb(242, 100, 0);
}
`);

(function() {
    handle_webcomic(document.querySelector('body > center:nth-child(3) > a:nth-child(2) > img, body > center:nth-child(3) > img'), document.querySelector('body > center:nth-child(3) > a:nth-child(4)'), document.querySelector('body > center:nth-child(3) > a:nth-child(5)'), true);
})();
