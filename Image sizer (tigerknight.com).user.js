// ==UserScript==
// @name         Image sizer (tigerknight.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(tigerknight.com).user.js
// @author       TilCreator
// @match        https://www.tigerknight.com/*/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.wrapper {width: 100% !important; max-width: 100%; !important}');

(function() {
    var first = true;
    document.querySelectorAll('.comic-image').forEach(function (image) {
        handle_webcomic(image, document.querySelector('nav > div > a:nth-child(2)'), document.querySelector('nav > div > a:nth-child(3)'), true, first);
        first = false;
    });
})();
