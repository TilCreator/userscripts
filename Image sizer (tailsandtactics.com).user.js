// ==UserScript==
// @name         Image sizer (tailsandtactics.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(tailsandtactics.com).user.js
// @author       TilCreator
// @match        https://tailsandtactics.com/comic/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('.wrapper, .bounds {width: 100% !important; max-width: none !important;}');

(function() {
    var first = true;
    document.querySelectorAll('img.comic-image, .comic-image img').forEach(function (image) {
        handle_webcomic(image, document.querySelector('.comic-nav > :nth-child(2)'), document.querySelector('.comic-nav > :nth-child(4)'), true, first);
        first = false;
    });
})();
