// ==UserScript==
// @name         Image sizer (xkcd.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(xkcd.com).user.js
// @author       TilCreator
// @match        https://xkcd.com/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`body {
  position: absolute !important;
  left: 0px !important;
  margin: 0px !important;
  width: 100% !important;
}
#middleContainer {
  position: fixed !important;
  left: 0px !important;
  margin: 0px !important;
  top: 0px !important;
  width: 100% !important;
  padding: 0px !important;
  border: none !important;
}
#comic img {
  image-orientation: none !important;
  width: 100% !important;
  height: calc(100vh - 1em) !important;
  object-fit: contain !important;
}
#middleContainer > .comicNav {
    display: none;
}
#middleContainer > .comicNav ~ .comicNav {
    display: block;
}
#ctitle {
    margin: 0px !important;
}`);

(function() {
    document.querySelector("#ctitle").innerHTML += ": " + document.querySelector("#comic img").title;
    handle_webcomic(document.querySelector('#comic img'), document.querySelector('.comicNav > li:nth-child(2) > a'), document.querySelector('.comicNav > li:nth-child(4) > a'));
})();
