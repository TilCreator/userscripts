// ==UserScript==
// @name         Image sizer (sandraandwoo.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(sandraandwoo.com).user.js
// @author       TilCreator
// @match        http*://www.sandraandwoo.com/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
#page {
  width: 100% !important;
}

* {
  background: #000 !important;
  color: #fff;
}

#comic img {
  /*filter: invert(1);*/
  width: 100%;
  /*max-height: 100vh;*/
  object-fit: contain;
}

#comic-wrap {
  padding: 0px;
}
`);

(function() {
    handle_webcomic(document.querySelector('#comic img'), document.querySelector('.nav-previous > a'), document.querySelector('.nav-next > a'));
})();
