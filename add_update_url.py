import os

for name in os.listdir('./'):
    if name.endswith('.js'):
        with open(name, 'r') as file:
            content = file.read()

        if '@downloadURL' in content:
            continue

        print(name)

        name_i = content.find('// @name')
        insert_i = content[name_i:].find('\n') + name_i + 1
        new_content = content[:insert_i] + f'// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/{name.replace(" ", "%20")}\n' + content[insert_i:]

        with open(name, 'w') as file:
            file.write(new_content)
