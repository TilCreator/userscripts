// ==UserScript==
// @name         Image sizer (poisonedminds.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(poisonedminds.com).user.js
// @author       TilCreator
// @match        http://www.poisonedminds.com/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle('img.ksc {max-width: 99vw;max-height: 200vh; width: 99vw !important; height: auto !important; margin: auto; object-fit: contain;} \
body {margin: 0px} \
body > font:nth-child(1) > div:nth-child(1), body > font:nth-child(1) > p:nth-child(2) {display: none}');

(function() {
    handle_webcomic(document.querySelector('img.ksc'), document.querySelector('a[rel=prev]'), document.querySelector('a[rel=next]'));
})();
