// ==UserScript==
// @name         Image sizer (twindragonscomic.com)
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Image%20sizer%20(twindragonscomic.com).user.js
// @author       TilCreator
// @match        https://www.twindragonscomic.com/comic/*
// @match        https://www.gamerdragons.com/comic/*
// @require      https://gitlab.com/TilCreator/userscripts/-/raw/master/webcomic-helper.js
// @grant        GM_addStyle
// ==/UserScript==

GM_addStyle(`
#page {
    margin: 0px;
    width: 100vw;
    max-width: 100vw;
}
#content-column {
    width: 100vw;
}
#comic {
    width: 100vw;
}
#comic img {
  max-width: 100vw;
  max-height: calc(100vh - 2em - 30px);
  width: 100vw;
  height: 100vh;
  object-fit: contain;
  position: relativ;
  z-index: 9000;
}
#comic > a {
    color: inherit !important;
}
#comic > a::after {
  padding: 5px;
  font-size: 16px;
  color: inherit !important;
}
`);

(function() {
    for (i = 1; i < document.querySelectorAll('#comic > *').length + 1; i += 1) {
        a = document.querySelector('#comic a:nth-child(' + i + ')');
        if (a != null) {
            img = a.querySelector('img');
            if (img != null && img.alt != undefined) {
                GM_addStyle('#comic a:nth-child(' + i + ')::after { content: "' + img.alt + '" }');
            }
        }
    }
    document.querySelectorAll('#comic img').forEach((img) => {
        if (img != document.querySelector('#comic > a:first-child > img')) {
            handle_webcomic(img, document.querySelector('a.comic-nav-previous'), document.querySelector('a.comic-nav-next'), false, false, 2, 3, false);
            img.onload = null;
        }
    });
    handle_webcomic(document.querySelector('#comic > a:first-child > img'), document.querySelector('a.comic-nav-previous'), document.querySelector('a.comic-nav-next'), false, true);
})();

