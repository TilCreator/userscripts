// ==UserScript==
// @name         Keymappings for e621
// @downloadURL  https://gitlab.com/TilCreator/userscripts/-/raw/master/Keymappings%20for%20e621.user.js
// @author       TilCreator
// @match        https://e621.net/posts/*
// ==/UserScript==


(function() {
    document.onkeydown = function (e) {
        console.log(document.activeElement);
        if (document.activeElement !== document.body) {
            return;
        }

        e = e || window.event;

        if (e.keyCode == '37' || e.keyCode == '68') {
            document.querySelector('.prev').click();
        } else if (e.keyCode == '39' || e.keyCode == '70') {
            document.querySelector('.next').click();
        }
    }
})();